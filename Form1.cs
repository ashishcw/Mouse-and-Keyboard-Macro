﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Windows.Input;
using System.Windows;
using System.Data.SqlClient;


namespace MouseMacroRecorder
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        private const int MOUSEEVENTF_MOVE = 0x0001;
        private const int MOUSEEVENTF_LEFTDOWN = 0x0002;
        private const int MOUSEEVENTF_LEFTUP = 0x0004;
        private int indextohold;
        ListViewItem lv;

        private LowLevelKeyboardListener _listener;

        int a, b, sleeptime;

        int PW = 0; //Initial Panel Width
        int MaxPanelWidth;//This is Maximum Panel Width
        bool PanelHided = true; // Default Status of Panel
        bool showPanel = false;
        string pastDate; //Past Date
        String currentDateToday; //Current Date String

        public string DateFrom; //This will hold the From-Date value
        public string DateTo; //This will hold the To-Date value
        public string TeamName; //This will hold the Team Name value
        public string ReportQuerytofetchStatus; //This will hold the Status value

        protected int LastPositionofListView;
        List<string> texttoEnterr;
        TextWriter temptext;
        string mainpath, mainpath2, temptime, tempposition1, tempposition2, currentstring;
        int templinestoread = 0;
        string strpath = Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
        private bool mouseclicked = false, AutoTimerchecked = false, KeysSent = false, textadded = false, returnkeypressed = false, KeysneedstobeSent = false;
        Stopwatch tempwatch1 = new Stopwatch();
        List<string> Texttoenter;
        [return: MarshalAs(UnmanagedType.Bool)]

        public int gettimercount = 0;
        

        private void Event(object sender, EventArgs e)
        {
            if (!mouseclicked && !button1.Enabled)
            {
                mouseclicked = true;

            }
        }

        private void Eventt(object sender, EventArgs e)
        {

        }

        public Form1()
        {
            InitializeComponent();

            gettimercount = 0;

            listView1.Enabled = true;
            listView1.Show();

            indextohold = listView1.Items.Count;

            mainpath2 = strpath + @"/MouseMacroMain.txt";
            temptext = new StreamWriter(mainpath2);

            numericUpDown1.Enabled = false;

            panel2.Width = PW;
            MaxPanelWidth = 290;

            MouseHook.Start();
            MouseHook.MouseAction += new EventHandler(Event);
            KeyBoardHook.Start();
            KeyBoardHook.KeyboardAction += new EventHandler(Eventt);

            LastPositionofListView = listView1.Items.Count - 1;

            texttoEnterr = new List<string>();

            button2.Enabled = false;
            button3.Enabled = true;
            button6.Enabled = false;
            button6.Hide();

            
            
        }

        void Loadingcomplete()
        {

            _listener = new LowLevelKeyboardListener();
            _listener.OnKeyPressed += _listener_OnKeyPressed;

            _listener.HookKeyboard();
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _listener = new LowLevelKeyboardListener();
            _listener.OnKeyPressed += _listener_OnKeyPressed;

            _listener.HookKeyboard();
        }


        void _listener_OnKeyPressed(object sender, KeyPressedArgs e)
        {
            _listener.UnHookKeyboard();
            _listener.HookKeyboard();

            if (e.KeyPressed == Key.Space)
            {
                this.richTextBox1.Text += " ";
            }
            //else if (e.KeyPressed == Key.LeftAlt || e.KeyPressed == Key.LeftShift || e.KeyPressed == Key.RightAlt || e.KeyPressed == Key.RightCtrl || e.KeyPressed == Key.RightShift || e.KeyPressed == Key.Home || e.KeyPressed == Key.Delete || e.KeyPressed == Key.Insert || e.KeyPressed == Key.End || e.KeyPressed == Key.PageDown || e.KeyPressed == Key.PageUp)
            else if (e.KeyPressed == Key.Home || e.KeyPressed == Key.Delete || e.KeyPressed == Key.Insert || e.KeyPressed == Key.End || e.KeyPressed == Key.PageDown || e.KeyPressed == Key.PageUp)
            {

            }
            else if (e.KeyPressed == Key.LeftCtrl || e.KeyPressed == Key.RightCtrl)
            {
                this.richTextBox1.Text += "^";
            }
            else if (e.KeyPressed == Key.LeftShift || e.KeyPressed == Key.RightShift)
            {
                this.richTextBox1.Text += "+";
            }
            else if (e.KeyPressed == Key.LeftAlt || e.KeyPressed == Key.RightAlt)
            {
                this.richTextBox1.Text += "%";
            }
            else if (e.KeyPressed == Key.LeftShift && e.KeyPressed == Key.D2)
            {
                this.richTextBox1.Text += "@";
            }
            else if (e.KeyPressed == Key.OemComma)
            {
                this.richTextBox1.Text += ",";
            }
            else if (e.KeyPressed == Key.Back)
            {
                //richTextBox1.Text = richTextBox1.Text.Remove(richTextBox1.Text.Length);
                //string alltext = this.richTextBox1.Text;
                //alltext.Remove(alltext.Length - 1);
            }
            else if (e.KeyPressed == Key.Return)
            {
                returnkeypressed = true;
            }
            //else if (e.KeyPressed == Key.Back)
            //{
            //    //this.richTextBox1.Text -= "";
            //}
            else if (e.KeyPressed == Key.OemPeriod)
            {
                this.richTextBox1.Text += ".";
            }
            else
            {
                string tempcharofkey = e.KeyPressed.ToString();


                this.richTextBox1.Text += tempcharofkey.ToLower();
            }
            //if ((Control.ModifierKeys & Keys.Space) == Keys.Space)
            //{

            //}
            //else
            //{
            //    var tempcharofkey = e.KeyPressed.ToString();
            //    tempcharofkey.ToLower();
            //    this.richTextBox1.Text += e.KeyPressed.ToString();
            //    this.richTextBox1.Text += tempcharofkey.ToLower();
            //}




        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _listener.UnHookKeyboard();
        }


        public static class KeyBoardHook
        {
            private const int WH_KEYBOARD_LL = 13;

            private const int WM_KEYDOWN = 0x0100;

            private static LowLevelKeyboardProc _proc = HookCallback;

            private static IntPtr _hookID = IntPtr.Zero;

            public static event EventHandler KeyboardAction = delegate { };

            public static void Start()
            {
                _hookID = SetHook(_proc);


            }
            public static void stop()
            {
                UnhookWindowsHookEx(_hookID);
            }


            private static IntPtr SetHook(LowLevelKeyboardProc proc)

            {

                using (Process curProcess = Process.GetCurrentProcess())

                using (ProcessModule curModule = curProcess.MainModule)

                {

                    return SetWindowsHookEx(WH_KEYBOARD_LL, proc,

                        GetModuleHandle(curModule.ModuleName), 0);

                }

            }

            private delegate IntPtr LowLevelKeyboardProc(

       int nCode, IntPtr wParam, IntPtr lParam);


            private static IntPtr HookCallback(

                int nCode, IntPtr wParam, IntPtr lParam)

            {

                if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)

                {

                    int vkCode = Marshal.ReadInt32(lParam);

                    Console.WriteLine((Keys)vkCode);

                }

                return CallNextHookEx(_hookID, nCode, wParam, lParam);

            }

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

            private static extern IntPtr SetWindowsHookEx(int idHook,

        LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);


            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

            [return: MarshalAs(UnmanagedType.Bool)]

            private static extern bool UnhookWindowsHookEx(IntPtr hhk);


            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

            private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,

                IntPtr wParam, IntPtr lParam);


            [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]

            private static extern IntPtr GetModuleHandle(string lpModuleName);

        }

        public static class MouseHook
        {
            public static event EventHandler MouseAction = delegate { };

            public static void Start()
            {
                _hookID = SetHook(_proc);


            }
            public static void stop()
            {
                UnhookWindowsHookEx(_hookID);
            }

            private static LowLevelMouseProc _proc = HookCallback;
            private static IntPtr _hookID = IntPtr.Zero;

            private static IntPtr SetHook(LowLevelMouseProc proc)
            {
                using (Process curProcess = Process.GetCurrentProcess())
                using (ProcessModule curModule = curProcess.MainModule)
                {
                    return SetWindowsHookEx(WH_MOUSE_LL, proc,
                      GetModuleHandle(curModule.ModuleName), 0);
                }
            }

            private delegate IntPtr LowLevelMouseProc(int nCode, IntPtr wParam, IntPtr lParam);

            private static IntPtr HookCallback(
              int nCode, IntPtr wParam, IntPtr lParam)
            {
                if (nCode >= 0 && MouseMessages.WM_LBUTTONDOWN == (MouseMessages)wParam)
                {
                    MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
                    MouseAction(null, new EventArgs());
                }
                return CallNextHookEx(_hookID, nCode, wParam, lParam);
            }

            private const int WH_MOUSE_LL = 14;

            private enum MouseMessages
            {
                WM_LBUTTONDOWN = 0x0201,
                WM_LBUTTONUP = 0x0202,
                WM_MOUSEMOVE = 0x0200,
                WM_MOUSEWHEEL = 0x020A,
                WM_RBUTTONDOWN = 0x0204,
                WM_RBUTTONUP = 0x0205
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct POINT
            {
                public int x;
                public int y;
            }

            [StructLayout(LayoutKind.Sequential)]
            private struct MSLLHOOKSTRUCT
            {
                public POINT pt;
                public uint mouseData;
                public uint flags;
                public uint time;
                public IntPtr dwExtraInfo;
            }

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern IntPtr SetWindowsHookEx(int idHook,
              LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool UnhookWindowsHookEx(IntPtr hhk);

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
              IntPtr wParam, IntPtr lParam);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern IntPtr GetModuleHandle(string lpModuleName);


        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (a != b)
            {
                if (checkBox1.Checked == false)
                {                    
                    System.Windows.Forms.Cursor.Position = new System.Drawing.Point(int.Parse(listView1.Items[a].SubItems[0].Text), int.Parse(listView1.Items[a].SubItems[1].Text));

                    texttoEnterr.Add(listView1.Items[a].SubItems[3].Text);

                    MouseClick();
                    a++;
                    timer2.Stop();
                }
                else
                {
                    System.Windows.Forms.Cursor.Position = new System.Drawing.Point(int.Parse(listView1.Items[a].SubItems[0].Text), int.Parse(listView1.Items[a].SubItems[1].Text));


                    if (KeysSent == false)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            //SendKeys.SendWait(texttoEnterr[i]);

                        }
                        KeysSent = true;
                    }
                    int countasneeded = int.Parse(listView1.Items[a].SubItems[2].Text);

                    sleeptime = countasneeded * 1000;
                    MouseClick();
                    a++;
                    timer2.Stop();
                }
            }

            if (a == b)
            {
                button3.Enabled = true;
                
                if (MTTRCheckBox.Checked)
                {
                    System.Windows.Clipboard.Clear();
                    //System.Diagnostics.Process.Start("MTTRUTILITY.exe");
                }

            }
        }


        public new void MouseClick()
        {

            KeysneedstobeSent = false;
            ExecuteIn(500, () =>
            {
                Thread.Sleep(sleeptime);
            });
            ExecuteIn(1000, () =>
            {


                if (KeysneedstobeSent == false)
                {
                    LastPositionofListView = listView1.Items.Count - 1;
                    if (indextohold < LastPositionofListView)
                    {  
                        if (listView1.Items[a].SubItems[3].Text != null)
                        {
                            SendKeys.Send(listView1.Items[a].SubItems[3].Text);
                        }

                    }

                    KeysneedstobeSent = true;
                }

                mouse_event(MOUSEEVENTF_LEFTDOWN, System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y, 0, 0);
                mouse_event(MOUSEEVENTF_LEFTUP, System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y, 0, 0);


                timer2.Start();
            });
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            if (mouseclicked || returnkeypressed)
            {
                temptime = tempwatch1.Elapsed.Seconds.ToString();
                tempwatch1.Reset();
                if (!tempwatch1.IsRunning)
                {
                    tempwatch1.Start();
                }
                tempposition1 = System.Windows.Forms.Cursor.Position.X.ToString();
                tempposition2 = System.Windows.Forms.Cursor.Position.Y.ToString();

                lv = new ListViewItem(System.Windows.Forms.Cursor.Position.X.ToString());
                lv.SubItems.Add(System.Windows.Forms.Cursor.Position.Y.ToString());

                if (AutoTimerchecked)
                {
                    lv.SubItems.Add(temptime.ToString());


                    if (richTextBox1 != null)
                    {
                        texttoEnterr.Add(richTextBox1.Text);
                        textadded = true;


                        temptext.WriteLine(tempposition1 + "," + tempposition2 + "," + temptime.ToString() + "," + richTextBox1.Text);
                        if (textadded == true)
                        {
                            for (int i = 0; i < texttoEnterr.Count; i++)
                            {
                                lv.SubItems.Add(richTextBox1.Text);
                            }

                            textadded = false;
                        }

                        richTextBox1.Clear();
                    }
                    else
                    {
                        temptext.WriteLine(tempposition1 + "," + tempposition2 + "," + temptime.ToString() + "," + " ");
                    }


                }
                else
                {
                    string tmpsleeptime = sleeptime.ToString();

                    if (tmpsleeptime.Length > 3)
                    {
                        tmpsleeptime = tmpsleeptime.Remove(tmpsleeptime.Length - 3);
                    }
                    lv.SubItems.Add(tmpsleeptime.ToString());
                    temptext.WriteLine(tempposition1 + "," + tempposition2 + "," + tmpsleeptime.ToString());
                }



                listView1.Items.Add(lv);
                b++;

                mouseclicked = false;
                returnkeypressed = false;

            }

        }

        private void button1_Click(object sender, EventArgs e) //Record Button
        {
            System.Windows.Clipboard.Clear();

        }



        private void button2_Click(object sender, EventArgs e) //Stop Button
        {
            timer1.Stop();
            timer2.Stop();
            mouseclicked = false;
            button1.Enabled = true;
            temptext.Close();
            temptext.Dispose();
            //timer3.Stop();

            button2.Enabled = false;
            if (tempwatch1.IsRunning)
            {
                tempwatch1.Stop();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            a = 0;
            b = 0;

        }

        private void button4_Click(object sender, EventArgs e) //Load Macro Button
        {
            openFileDialog1 = new System.Windows.Forms.OpenFileDialog();


            string mainpathh = strpath;
            openFileDialog1.InitialDirectory = mainpathh;

            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                mainpath = openFileDialog1.FileName;

                string[] lines = File.ReadAllLines(mainpath);

                while ((templinestoread != lines.Count()))
                {
                    foreach (string line in lines)
                    {
                        string[] temp = new string[4];
                        temp[0] = "";
                        temp[1] = "";
                        temp[2] = "";
                        temp[3] = "";
                        temp = line.Split(new char[] { ',' });
                        string xCoord = temp[0];
                        string yCoord = temp[1];
                        string timetowait = temp[2];
                        string TexttoType = temp[3];
                        lv = new ListViewItem(xCoord);
                        lv.SubItems.Add(yCoord);
                        lv.SubItems.Add(timetowait);
                        if (TexttoType != null)
                        {
                            lv.SubItems.Add(TexttoType);
                        }
                        listView1.Items.Add(lv);

                        b++;

                        temptext.WriteLine(xCoord + "," + yCoord + "," + temptime + "," + texttoEnterr);
                        templinestoread++;
                    }

                }

            }

        }



        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e) //Clear Button
        {
            if (richTextBox1.Text.Length > 0)
            {
                lv.ListView.Items.Clear();
                System.Windows.Forms.MessageBox.Show("Data Cleared");
            }
            else
            {
                return;
            }
        }

        private void button1_Click_1(object sender, EventArgs e) //Record Button
        {
            button1.Enabled = false;
            button2.Enabled = true;
            checkBox1.Enabled = false;
            timer1.Start();
            if (!tempwatch1.IsRunning)
            {
                tempwatch1.Start();
            }
            //timer3.Start();

            Loadingcomplete();
        }

        private void MTTRCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            timer3.Start();
            if (!showPanel)
            {
                showPanel = true;
            }
        }

        private void MTTRCalculationBtn_Click(object sender, EventArgs e)
        {
//            System.Diagnostics.Process.Start("MTTRUTILITY.exe");
//            MTTRCalculationBtn.Enabled = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void About_btn_Click(object sender, EventArgs e)
        {
           System.Windows.MessageBox.Show("This Utility is created by Ashish R");
        }

        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e) //Weekly Button Panel
        {
            
            
        //        dateTimePicker1.Format = DateTimePickerFormat.Custom;
        //        dateTimePicker1.CustomFormat = "dd-MM-yyyy";


        //        dateTimePicker2.Format = DateTimePickerFormat.Custom;
        //        dateTimePicker2.CustomFormat = "dd-MM-yyyy";


        //        pastDate = DateTime.Now.AddDays(-07).ToShortDateString();

        //        currentDateToday = DateTime.Now.ToShortDateString();

        //        dateTimePicker1.Text = pastDate;
        //        dateTimePicker2.Text = currentDateToday;            
        }

        private void button8_Click(object sender, EventArgs e) //Monthly Button Panel
        {
            //dateTimePicker1.Format = DateTimePickerFormat.Custom;
            //dateTimePicker1.CustomFormat = "dd-MM-yyyy";


            //dateTimePicker2.Format = DateTimePickerFormat.Custom;
            //dateTimePicker2.CustomFormat = "dd-MM-yyyy";


            //pastDate = DateTime.Now.AddDays(-30).ToShortDateString();

            //currentDateToday = DateTime.Now.ToShortDateString();

            //dateTimePicker1.Text = pastDate;
            //dateTimePicker2.Text = currentDateToday;
        }

        private void button9_Click(object sender, EventArgs e) // 45 Days Button Panel
        {
            //dateTimePicker1.Format = DateTimePickerFormat.Custom;
            //dateTimePicker1.CustomFormat = "dd-MM-yyyy";


            //dateTimePicker2.Format = DateTimePickerFormat.Custom;
            //dateTimePicker2.CustomFormat = "dd-MM-yyyy";


            //pastDate = DateTime.Now.AddDays(-45).ToShortDateString();

            //currentDateToday = DateTime.Now.ToShortDateString();

            //dateTimePicker1.Text = pastDate;
            //dateTimePicker2.Text = currentDateToday;
        }

        private void button10_Click(object sender, EventArgs e) //Okay Button Panel
        {
            ////string AssignedGroup1 = "'Assigned Group*";            
            //string AssignedGroup1 = "'Assigned Group";
            //string AssignedGroup2 = "{+}";
            //string AssignedGroup3 = "'";
            //string Equalswithspace = " = ";
            //string ANDKeyValue = " AND ";
            //string LastResolvedDateLessThan = "'Last Resolved Date' <= ";
            //string LastResolvedDateMoreThan = "'Last Resolved Date' >= ";
            //string DoubleColumnsforDates = ("\"");
            ////string StatusKeyValue = "'Status*' = ";
            //string StatusKeyValue = "'Status' = ";


            //TeamName = this.comboBox1.GetItemText(this.comboBox1.SelectedItem);
            //ReportQuerytofetchStatus = this.comboBox2.GetItemText(this.comboBox2.SelectedItem);
            //DateFrom = this.dateTimePicker1.Text;
            //DateTo = this.dateTimePicker2.Text;
            //string Querytopaste;
            ////string Querytocheck = ("All");

            //string Querytocheck = DoubleColumnsforDates + ("All") + DoubleColumnsforDates;
            ////Querytopaste = AssignedGroup1 + AssignedGroup2 + AssignedGroup3 + Equalswithspace + TeamName + ANDKeyValue + LastResolvedDateMoreThan + DoubleColumnsforDates + DateFrom + DoubleColumnsforDates + ANDKeyValue + LastResolvedDateLessThan + DoubleColumnsforDates + DateTo + DoubleColumnsforDates + ANDKeyValue + StatusKeyValue + ReportQuerytofetchStatus;

            //Querytopaste = AssignedGroup1 + AssignedGroup3 + Equalswithspace + TeamName + ANDKeyValue + LastResolvedDateMoreThan + DoubleColumnsforDates + DateFrom + DoubleColumnsforDates + ANDKeyValue + LastResolvedDateLessThan + DoubleColumnsforDates + DateTo + DoubleColumnsforDates + ANDKeyValue + StatusKeyValue + ReportQuerytofetchStatus;

            //System.Windows.Clipboard.Clear();
            //System.Windows.Clipboard.SetText(Querytopaste);


        }

        private void button11_Click(object sender, EventArgs e) //Cancel Button Panel
        {
            if (MTTRCheckBox.Checked)
            {
                MTTRCheckBox.Checked = false;
            }            
        }
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }       

        void EnterKeyFunction(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                texttoEnterr.Add(richTextBox1.Text);
                textadded = true;

                temptext.WriteLine(tempposition1 + "," + tempposition2 + "," + temptime.ToString() + "," + richTextBox1.Text);
                if (textadded == true)
                {
                    for (int i = 0; i < texttoEnterr.Count; i++)
                    {
                        lv.SubItems.Add(richTextBox1.Text);
                    }

                    textadded = false;
                }

                richTextBox1.Clear();
            }

        }

        private List<string> tempcharlist = new List<string>();



        private void timer3_Tick(object sender, EventArgs e) //Panel Timer
        {
            if (showPanel && PanelHided)
            {
                panel2.Width += 230;

                if (panel2.Width >= MaxPanelWidth)
                {
                    timer3.Stop();
                    PanelHided = false;
                    this.Refresh();
                }

            }
            else
            {
                panel2.Width -= 230;
                if (panel2.Width <= 0)
                {
                    timer3.Stop();
                    PanelHided = true;
                    this.Refresh();
                }
            }

            //if (MTTRCheckBox.Checked && PanelHided)
            //{
            //    panel2.Width += 230;

            //    if (panel2.Width >= MaxPanelWidth)
            //    {
            //        timer3.Stop();
            //        PanelHided = false;
            //        this.Refresh();
            //    }
            //}
            //else if (MTTRCheckBox.Checked && !PanelHided)
            //{
            //    panel2.Width -= 230;
            //    if (panel2.Width <= 0)
            //    {
            //        timer3.Stop();
            //        PanelHided = true;
            //        this.Refresh();
            //    }
            //}

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            int tempsleeptime = (int)numericUpDown1.Value;
            sleeptime = tempsleeptime * 1000;
        }

        private void button3_Click(object sender, EventArgs e) //Play Button
        {
            a = 0;
            timer2.Start();
            button3.Enabled = true;


        }

        private void richTextBox1_TextChanged(object sender, EventArgs e) //Text to Enter Form Field
        {

        }

        private void button6_Click(object sender, EventArgs e) //Add Text Button
        {
            //texttoEnterr.Add(richTextBox1.Text);
            //textadded = true;


            //temptext.WriteLine(tempposition1 + "," + tempposition2 + "," + temptime.ToString() + "," + richTextBox1.Text);
            //if (textadded == true)
            //{
            //    for (int i = 0; i < texttoEnterr.Count; i++)
            //    {
            //        lv.SubItems.Add(richTextBox1.Text);
            //    }

            //    textadded = false;
            //}

            //richTextBox1.Clear();


        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e) //Loop Tab
        {
            if (checkBox2.Checked)
            {
                numericUpDown1.Enabled = true;

            }
            else
            {
                numericUpDown1.Enabled = false;
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) //Auto Timer Check Box
        {
            if (checkBox1.Checked)
            {
                AutoTimerchecked = true;

            }
            else
            {
                AutoTimerchecked = false;

            }
        }

        public static void ExecuteIn(int milliseconds, Action action)
        {
            var timer = new System.Windows.Forms.Timer();
            timer.Tick += (s, e) => { action(); timer.Stop(); };
            timer.Interval = milliseconds;
            timer.Start();
        }
    }
}
