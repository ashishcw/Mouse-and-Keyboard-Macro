# Mouse-and-Keyboard-Macro
This utility was purely made to automate the monotonous task. So it was completely made for fun and for education purposes. This will be useful mostly for people who do monotonous work. It has similar approach of Macro in an Excel.

# About This Tool
Custom classes are available to read mouse cursor X and Y Onscreen Positions, Single/Double Left Mouse clicks, Input Key string from keyboard hook, which is responsible for reading key press on events.

# DemoFile & Features
Demo file is availabe in MouseMacroRecorder.zip, one just simply have to execute the file.

Record Button - Once clicked, it will simply start to record the mouse movement and Keyboard input from user.
Stop Button - This will stop the timer and the macro will be saved till this point. Macro will simply consist Mouse Pointer X & Y Onscreen position everytime the click is performed and Keyboard inputs.
Play Button - User will be offered with a pop up window, where, user must select the earlier saved macro. If the right file is given as an input, system will automatically start to perform all the actions recorded earlier. E.g. Mouse clicks at diffrent positions, Keyboard input etc.
